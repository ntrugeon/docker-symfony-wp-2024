## Version de Windows
- Vous devez exécuter Windows 10 versions 2004 et ultérieures (build 19041 et versions ultérieures) ou Windows 11 pour utiliser les commandes ci-dessous.

## Installation de WSL 2

1. 

```sh
# dans un powershell
wsl --install
```

1. Installer la distribution **debian** via le store ou vous pouvez taper la commande
```sh
wsl --install -d debian
```
1. On vérifie que tout est bien installé
```sh
wsl --list --online
```

1. Lancer debian et installer git vim et make

```sh
sudo apt update
sudo apt upgrade
sudo apt install git vim make
```

1.  Avoir [Docker](https://www.docker.com/get-started/)
1. Dans Docker-Desktop, activer dans les paramètres l'integration de wsl pour debian. Cela permettra d'avoir les commandes docker dans debian
1. Modifier la config de docker dans debian (~/.docker/config.json)
   
```sh
# credsSTore doit être vide
credsStore:""
...
```

1. Avoir Phpstorm. Votre statut d'étudiant vous donne droit à une licence gratuite de la suite.
1. Installer les plugins symfony, wordpress et .env pour Phpstorm


## Installation

1. Placez vous dans la machine **debian**
1. Vous **ne devez pas** être root ni utiliser la commande sudo
1. Dupliquer le projet :
    ```bash
    git clone https://gitlab.univ-lr.fr/ntrugeon/docker-symfony-wp-2024.git devPhpLP
    # on se place dans le bon dossier
    cd devPhpLP
    ```

1. Construire et exécuter les conteneurs (Cela peut prendre un peu de temps)

    ```bash
    $ make build
    $ make up
    ```

## Choix du projet à installer 

* Installer Symfony
    1. Toujours sous **debian**, on installe symfony en version minimale grâce au Makefile.
        
        ```bash
        $ make newSF nom_de_mon_projet
        ```
        - cela va créer le projet via composer, un virtualhost ainsi qu'une base de données dédiée

    1.  On installe les composants nécessaires à nos applications également avec Composer

        ```bash
        $ make bash
        $ cd nom_de_mon_projet
        $ composer require --dev profiler maker
        $ composer require twig orm form validator
        ```
    
    1. Sous **windows** avec votre editeur de texte favori
       - Modifiez le fichier C:\Windows\System32\drivers\etc\hosts en ajoutant à la dernière ligne :
          >  127.0.0.1    nom_de_mon_projet.localhost

    1. Sous **windows**, ouvrir le dossier symfony du projet dans phpStorm. Il va reconnaître automatiquement le WSL et le termnianl sera par défaut sous debian ;)

* Installer Wordpress
   1.  Toujours sous **debian**, on installe wordpress grâce au Makefile.
        
        ```bash
        # pas de majuscules dans le nom du projet sinon les permaliens ne fonctionneront pas.
        $ make newWP nom_de_mon_projet
        ```
        - cela va créer le projet via composer, un virtualhost ainsi qu'une base de données dédiée

   1. Sous **windows** avec votre editeur de texte favori
       - Modifiez le fichier C:\Windows\System32\drivers\etc\hosts en ajoutant à la dernière ligne :
          >  127.0.0.1    nom_de_mon_projet.localhost
    
   1. Sous **windows**, ouvrir le dossier wordpress du projet dans phpStorm. Il va reconnaître automatiquement le WSL et le termnianl sera par défaut sous debian ;)
      - modifier **.env** 
        1. en recopiant les informations de https://roots.io/salts.html dans la partie concernée (AUTH_KEY, SECURE_AUTH_KEY, ...)
        2. en précisant le suffixe des tables : décommentez la variable DB_PREFIX et mettez le préfixe de votre choix.

* C'est parti :-)

## Je commence à travailler sur un projet

Vous avez juste à exécuter `make up`, puis:

* Ouvrez votre projet avec phpStorm
* Ouvrez votre navigateur [nom_de_mon_projet.localhost:8000](http://nom_de_mon_projet.localhost:8000)
* Logs du serveur web : logs/apache2

## Je finis de travailler sur le projet
Vous avez juste à exécuter `make down`.

## Comment cela fonctionne ?

Vous pouvez aller regarder le fichier `compose.yaml`, avec les images `docker compose` correspondantes:

* `db`: le container mariadb 10.11,
* `php`: php-fpm en version 8.3,
* `apache`: le serveur web caddy sur le port 8000,
* `mailer`: un serveur/client mail pour tester l'envoi de mail sur le port 8025,
* `adminer`: un outil pour administrer vos bases de données sur port 8306.

## Commandes utiles

```bash
# On rentre dans un conteneur en bash
$ make bash

# Commandes symfony
$ make bash
$ cd nom_de_mon_projet
$ sf make:controller
$ sf make:entity
$ composer req composant_utile
...

# Supprimer tous les conteneurs (en cas de gros plantage, à utiliser en dernier recours)
$ make cleanAll
```

## FAQ
* Je ne comprends rien, que faire ?
Allez voir votre prof !

* Xdebug?
Xdebug est déjà configuré
Il faut ajouter le module Xdebug helper pour Firefox ou pour Chrome
Il faut également configurer Phpstorm en se connectant au port  `9001` avec l'id `PHPSTORM`. Vous pouvez suivre ce [lien](https://blog.eleven-labs.com/fr/debug-run-phpunit-tests-using-docker-remote-interpreters-with-phpstorm/). Le dépôt que vous utilisez est déjà paramétré. Utilisez docker-compose à la place de docker dans le "Remote" de l'interpréteur PHP.
